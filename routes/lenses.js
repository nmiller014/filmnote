var express = require('express');
var router = express.Router();

/* GET Userlist page. */
router.get('', function(req, res) {
    var db = req.db;
    var collection = db.get('lenses');
    collection.find({},{},function(e,docs){
        res.render('lenses/index', {
            title: "Lenses",
            "lenses" : docs
        });
    });
});

router.get('/add', function(req, res) {
  res.render('lenses/add', { title: "Add a lens"});
});

/* POST to Add User Service */
router.post('/add', function(req, res) {

    // Set our internal DB variable
    var db = req.db;

    // Get our form values. These rely on the "name" attributes
    var name = req.body.name;
    var max_aperture = req.body.max_aperture;

    // Set our collection
    var lenses = db.get('lenses');

    // Submit to the DB
    lenses.insert({
        "name" : name,
        "max_aperture" : max_aperture
    }, function (err, doc) {
        if (err) {
            res.status(500);
            res.render('error', { error: err });
        }
        else {
            // And forward to success page
            res.redirect("/lenses");
        }
    });
});

router.get('/:lens_id', function(req, res) {
    console.log("Editing " + req.params.lens_id);
    var lens_id = req.params.lens_id;
    var db = req.db;
    var lenses = db.get('lenses').findById(lens_id, function(err, docs) {
        res.render('lenses/add', { title: "Edit lens", editing: true, lens: docs });
    });
});

router.post('/:lens_id', function(req, res) {
    var db = req.db;
    if (req.body.delete)
    {
        console.log("Deleting " + req.params.lens_id);

        

        res.redirect("/lenses");
    }
    else
    {
        var lenses = db.get('lenses').updateById(req.params.lens_id,
            {
                $set: {
                    name: req.body.name,
                    max_aperture: req.body.max_aperture
                }
            },
            function(err) {
                if (err) {
                    res.status(500);
                    res.render('error', { error: err });
                }
                else {
                    var lenses = db.get('lenses').findById(req.params.lens_id, function(err, docs) {
                        res.render('lenses/add', { title: "Edit lens", editing: true, lens: docs, success: true });
                    });
                }
            });
    }
});

module.exports = router;
