# Filmnote

A web-based app for recording film photography notes, shot-by-shot.

## Running filmnote

To run filmnote, make sure you have a mongo instance running and that the config.json file has been updated to point to it. The sample config points to the local server, which can be started by running

`npm run mongod`

once the mongo db connection is configured and mongo is started, you can start filmnote by running

`npm start`

## Data structure

This app allows you to store information about each shot in a structured manner. The following list defines the available records and their properties:

* Lens
  * Name
  * Max aperture
* Filter
  * Name
* Body
  * Format
  * Max shutter
* Roll
  * Name
  * ISO
  * Chemistry
  * Format
  * Body ID
* Shot
  * Aperture
  * Exposure time
  * Exposure value
  * Focal length
  * AF or MF?
  * Flash?
  * Lighting
  * Subject description
  * Date/time
  * Location (as GPS coords)
  * Roll ID
  * Filter ID
  * Lens ID